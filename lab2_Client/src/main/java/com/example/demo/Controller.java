package com.example.demo;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextArea;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.server.PWA;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.concurrent.Exchanger;


@Route("")
public class Controller extends VerticalLayout {

    TextArea textArea = new TextArea();
    public Controller(){
        add(textArea);
        textArea.setValue(some());

    }
    //http://172.17.0.2
    public String some(){
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<String> rateResponse =
                restTemplate.exchange("http://api:8080/get",
                        HttpMethod.GET,
                        HttpEntity.EMPTY,
                        new ParameterizedTypeReference<String>() {
                        });
        String rates = rateResponse.getBody();
        return rates;
    }
}
