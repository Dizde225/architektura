package lab2_api.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import java.util.ArrayList;
import java.util.List;

@Controller
public class CarApi {

    @Autowired
    private CarRepository carRepository;

    public String getCars()
    {
        return  carRepository.findAll().toString();
    }

    public void  addCar(Car car)
    {
        carRepository.save(car);
    }
}
