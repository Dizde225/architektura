package lab2_api.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CarController {

    @Autowired
    private CarApi carapi;

    @GetMapping("/get")
    public String get ()
    {
        return carapi.getCars();
    }

    @PostMapping("/add")
    public void  add(@RequestBody Car car)
    {
        carapi.addCar(car);
    }
}
