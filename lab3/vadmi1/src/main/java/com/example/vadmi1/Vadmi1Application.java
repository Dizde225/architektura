package com.example.vadmi1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Vadmi1Application {

    public static void main(String[] args) {
        SpringApplication.run(Vadmi1Application.class, args);
    }

}
