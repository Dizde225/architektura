package com.example.vadmi1;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextArea;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.router.Route;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

@Route("")
public class Client extends VerticalLayout {

    @Autowired
    private RabbitTemplate rabbitTemplate;
    private static List<String> messages = new ArrayList<>();

    private TextArea messagesTA = new TextArea();
    private Button refresh = new Button("refresh");
    private TextField messageContent = new TextField();
    private Button send = new Button("send");

     public Client(){
         add(messagesTA);
         add(refresh);
         add(messageContent);
         add(send);

         messagesTA.setWidth("1000px");
         messagesTA.setHeight("400px");


         refresh.addClickListener(clickEvent ->
                 setText());

         send.addClickListener(clickEvent ->
         {
                 get();
         });

     }

    public void setText(){
        get1();
        messagesTA.setValue(buildMessages());
    }

    public String buildMessages(){
        StringBuilder sb = new StringBuilder();
        for (String s : messages){
            sb.append(s);
        }
        return sb.toString();
    }

    public void get1()
    {
        Object message = "";
        while (message != null){
            message = rabbitTemplate.receiveAndConvert("cokolwiek");
            if (message != null)
            messages.add(message.toString());
        }
    }

    public void get()   {
        // setText();
        String s = messageContent.getValue();
        String ss= LocalTime.now().toString()+"\tUser2: "+s+"\n";
        rabbitTemplate.convertAndSend("cokolwiek1", ss);
        messages.add(ss);
        setText();
    }
}
