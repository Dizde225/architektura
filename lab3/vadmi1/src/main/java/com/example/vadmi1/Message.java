package com.example.vadmi1;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Date;

public class Message {
    private String content;
    private String date;

    public Message(String content) {
        this.content = content;
        this.date = LocalDate.now().toString()+" "+LocalTime.now().toString();
    }

    @Override
    public String toString() {
        return date + " " + content +"\n";
    }
}
