package ap1.ap1;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;
import java.time.LocalTime;

@RestController
public class MessageAdder {

    private RabbitTemplate rabbitTemplate;

    @Autowired
    public MessageAdder(RabbitTemplate rabbitTemplate) {
        this.rabbitTemplate = rabbitTemplate;
    }

    @GetMapping("/add")
    public String get(@RequestParam String s){
        String ss= LocalDate.now().toString()+" "+ LocalTime.now().toString()+"\t "+s+"\n";
        rabbitTemplate.convertAndSend("cokolwiek", ss);
        return "poszlo";
    }
}
