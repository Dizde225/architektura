package com.example.acrhitekturahealthcheck;

import org.springframework.boot.actuate.health.AbstractHealthIndicator;
import org.springframework.boot.actuate.health.Health;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;

@Component
public class CheckCatName extends AbstractHealthIndicator {
    final private String url = "http://localhost:8081/cats";

    @Override
    protected void doHealthCheck(Health.Builder builder) throws Exception {
        RestTemplate restTemplate = new RestTemplate();
        try {
            restTemplate.exchange(url, HttpMethod.GET, HttpEntity.EMPTY, String.class);
            builder.up();
        } catch (ResourceAccessException exception) {
            builder.down().withDetail("What happend", "Ups something is wrong!");
        }
    }
}
