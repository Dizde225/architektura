package com.example.acrhitekturahealthcheck;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AcrhitekturaHealthCheckApplication {

    public static void main(String[] args) {
        SpringApplication.run(AcrhitekturaHealthCheckApplication.class, args);
    }

}
