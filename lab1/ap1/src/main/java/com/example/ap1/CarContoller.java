package com.example.ap1;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CarContoller {

    @Autowired
    CarApi carApi;

    @GetMapping("/cars")
    public String getCars(){
        return carApi.getCars();
    }

    @PostMapping("/add")
    public void addCar(@RequestBody Car car){
        carApi.addCar(car);
    }
}
